from django.test import TestCase, Client
from .apps import AccountsConfig
from django.apps import apps

# Create your tests here.
class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(AccountsConfig.name, 'accounts')
        self.assertEqual(apps.get_app_config('accounts').name, 'accounts')


class Story9UnitTest(TestCase):
    def test_homepage_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)