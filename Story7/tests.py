from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest


class TestStory7(TestCase):
    def test_url_story7(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_template_story7(self):
        response = Client().get('/Story7/')
        self.assertTemplateUsed(response, 'index.html')

    def test_views_story7(self):
        found = resolve('/Story7/')
        self.assertEqual(found.view_name, "Story7.views.index")

    def test_check_landing_page_have_header(self):
        response = Client().get('/Story7/')
        content = response.content.decode('utf8')
        self.assertIn('<div class="item-header"', content)
        self.assertIn('&#8595;', content)
        self.assertIn('&#8593;', content)
        self.assertIn('<p class="text"', content)

